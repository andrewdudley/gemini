'use strict';

angular.module('gemini')
.factory('Session', function ($http, $rootScope, $localStorage, DARWIN_HOST) {

  return {
    signIn: function (email, password, callback) {

      $http.post(DARWIN_HOST + '/sessions/', { email: email, password: password })
      .success(function (data, status) {
        $rootScope.session = data;
        $localStorage.session = data;
        callback(status, data);
      })
      .error(function (data, status) {
        callback(status, data);
      });
    }
  };
});
