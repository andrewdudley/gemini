'use strict';

angular.module('gemini')
.directive('geminiCompanyForm', function (Company) {

  function link(scope) { // link(scope, element, attrs)

    if (scope.company.id) {
      Company.getCompany(scope.company.id, function (data) {

        scope.company.id = data.id;
        scope.company.name = data.name;

      }, function (data, status) {
        console.log('There was an error.');
        console.log(data);
        console.log(status);
      });
    }

    function killMeNow(didUpdate) {
      scope.$destroy();
      if (scope.company.closeCompanyForm) {
        scope.company.closeCompanyForm(didUpdate);
      }
    }

    scope.submitCompany = function (companyForm) {

      if (companyForm.$valid) {
        var company = {
          id: scope.company.id,
          name: scope.company.name
        };

        if (scope.company.id) {
          Company.updateCompany(company, function () {
            killMeNow(true);

          }, function (data, status) {
            console.log('There was an error.');
            console.log(data);
            console.log(status);
          });

        } else {
          Company.createCompany(company, function () {
            killMeNow(true);

          }, function (data, status) {
            console.log('There was an error.');
            console.log(data);
            console.log(status);
          });
        }
      }
    };

    scope.cancelCompanyForm = function () {
      killMeNow();
    };
  }

  return {
    link: link,
    replace: true,
    scope: {
      company: '=binding'
    },
    templateUrl: '/components/company-form/company-form.html'
  };
});
