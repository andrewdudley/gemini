'use strict';

var Toast = function ($mdToast) {
  var availableTypes = ['success', 'warning', 'info', 'alert', 'secondary'];

  function show(type, message) {
    if (availableTypes.indexOf(type) === -1) {
      throw 'Invalid message type';
    }

    $mdToast.show(
      $mdToast.simple()
        .content(message)
        .position('bottom left right')
        .hideDelay(10000)
    );
  }

  return {
    show: show
  };
};

Toast.$inject = ['$mdToast'];

angular.module('gemini').service('Flash', Toast);
