'use strict';

angular.module('gemini')
.factory('Company', function ($http, DARWIN_HOST) {

  return {

    createCompany: function (company, success, error) {

      $http.post(DARWIN_HOST + '/companies', company)
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    deleteCompany: function (companyId, success, error) {

      $http.delete(DARWIN_HOST + '/companies/' + companyId)
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    getCompany: function (companyId, success, error) {

      $http.get(DARWIN_HOST + '/companies/' + companyId)
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    getCompanies: function (success, error) {

      $http.get(DARWIN_HOST + '/companies')
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    updateCompany: function (company, success, error) {

      $http.put(DARWIN_HOST + '/companies/' + company.id, company)
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    }

  };
});
