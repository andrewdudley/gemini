'use strict';

angular.module('gemini')
.directive('geminiUserForm', function (User) {

  function link(scope) { // link(scope, element, attrs)

    User.getRoles(function(roles) {
      console.log('Roles:');
      console.log(roles);
      scope.roles = roles;

    }, function (data, status) {
      console.log('error');
      console.log(data);
      console.log(status);
    });

    if (scope.user.id) {
      User.getUser(scope.user.id, function (data) {

        scope.user.id = data.id;
        scope.user.email = data.email;
        scope.user.firstName = data.firstName;
        scope.user.lastName = data.lastName;

      }, function (data, status) {
        console.log('There was an error.');
        console.log(data);
        console.log(status);
      });
    }

    function killMeNow(didUpdate) {
      scope.$destroy();
      if (scope.user.closeUserForm) {
        scope.user.closeUserForm(didUpdate);
      }
    }

    scope.submitUser = function (userForm) {

      if (userForm.$valid) {
        var user = {
          id: scope.user.id,
          email: scope.user.email,
          firstName: scope.user.firstName,
          lastName: scope.user.lastName,
          password: scope.user.password
        };

        if (scope.user.id) {
          User.updateUser(user, function () {
            killMeNow(true);

          }, function (data, status) {
            console.log('There was an error.');
            console.log(data);
            console.log(status);
          });

        } else {
          User.createUser(user, function () {
            killMeNow(true);

          }, function (data, status) {
            console.log('There was an error.');
            console.log(data);
            console.log(status);
          });
        }
      }
    };

    scope.cancelUserForm = function () {
      killMeNow();
    };
  }

  return {
    link: link,
    replace: true,
    scope: {
      user: '=binding'
    },
    templateUrl: '/components/user-form/user-form.html'
  };
});
