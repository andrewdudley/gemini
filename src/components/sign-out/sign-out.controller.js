'use strict';

angular.module('gemini')
.controller('SignOutCtrl', function ($rootScope, $localStorage, $location) {

  console.log('Signing out.');
  delete $rootScope.session;
  delete $localStorage.session;

  $location.path('/');
});
