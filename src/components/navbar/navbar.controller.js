'use strict';

angular.module('gemini')
.controller('NavbarCtrl', function ($scope, $rootScope, $localStorage, $location) {

  $scope.signOut = function () {

    console.log('Signing out.');
    delete $rootScope.session;
    delete $localStorage.session;

    $location.path('/');
  };
});
