'use strict';

angular.module('gemini')
.directive('geminiNavbar', function () {

  return {
    replace: true,
    scope: true,
    templateUrl: '/components/navbar/navbar.html'
  };
});
