'use strict';

angular.module('gemini')
.factory('User', function ($http, DARWIN_HOST) {

  return {

    getRoles: function (success, error) {

      $http.get(DARWIN_HOST + '/users/roles')
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    createUser: function (user, success, error) {

      $http.post(DARWIN_HOST + '/users', user)
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    deleteUser: function (userId, success, error) {

      $http.delete(DARWIN_HOST + '/users/' + userId)
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    getUser: function (userId, success, error) {

      $http.get(DARWIN_HOST + '/users/' + userId)
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    getUsers: function (success, error) {

      $http.get(DARWIN_HOST + '/users')
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    updateUser: function (user, success, error) {

      $http.put(DARWIN_HOST + '/users/' + user.id, user)
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    }

  };
});
