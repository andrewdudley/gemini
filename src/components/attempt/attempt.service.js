'use strict';

angular.module('gemini')
.factory('Attempt', function ($http, DARWIN_HOST) {

  return {

    getAttemptsByCompanyId: function (companyId, success, error) {

      $http.get(DARWIN_HOST + '/attempts?company_id=' + companyId)
      .success(function (data) {
        console.log(data);
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    getScore: function (attemptId, success, error) {

      $http.get(DARWIN_HOST + '/attempts/' + attemptId)
      .success(function (data) {
        success(data);
      })
      .error(function (data, status) {
        error(data, status);
      });
    }

  };
});
