'use strict';

angular.module('gemini')
  .controller('MainCtrl', function ($scope, $location, Flash, Session) {

    $scope.signIn = function (email, password) {
      console.log('email: ' + email);
      console.log('password: ' + password);

      Session.signIn(email, password, function (status, data) {
        console.log('status:');
        console.log(status);
        if (status === 200) {
          console.log('Forwarding to dashboard');
          $location.path('/dashboard');

        } else if (status === 401 || status === 404) {
          console.log('made it here');
          Flash.show('warning', 'Either your username or password was incorrect. Please try again.');

        } else {
          Flash.show(data.message);
        }
      });
    };

  });
