'use strict';

angular.module('gemini')
.controller('DashboardCtrl', function ($scope, $rootScope, $location, $compile, Flash, Attempt, Company, User) {

  $scope.loadUsers = function () {
    User.getUsers(function (users) {
      console.log('Users:');
      console.log(users);
      $scope.users = users;

    }, function (data, status) {
      if (status === 401) {
        delete $rootScope.session;
        $location.path('/');

      } else {
        Flash.show(data.message);
      }
    });
  };

  $scope.loadCompanies = function () {
    Company.getCompanies(function (companies) {
      console.log('Companies:');
      console.log(companies);
      $scope.companies = companies;

    }, function (data, status) {
      if (status === 401) {
        delete $rootScope.session;
        $location.path('/');

      } else {
        Flash.show(data.message);
      }
    });
  };

  $scope.deleteUser = function (userId) {
    User.deleteUser(userId, function () {
      $scope.loadUsers();

    }, function (data, status) {
      console.log('error');
      console.log(data);
      console.log(status);
    });
  };

  function addUserFormToDashboard(userId) {
    $scope.user = $scope.$new();
    var compiledDirective = $compile('<gemini-user-form binding="user"></gemini-user-form>');
    var directiveElement = compiledDirective($scope.user);
    angular.element('#edit-user-container').append(directiveElement);

    $scope.user.closeUserForm = function (didUpdate) {
      angular.element('#edit-user-container').empty();
      $scope.showNewUserButton = true;
      if (didUpdate) {
        $scope.loadUsers();
      }
    };
    $scope.user.id = userId;
  }

  function addCompanyFormToDashboard(companyId) {
    $scope.company = $scope.$new();
    var compiledDirective = $compile('<gemini-company-form binding="company"></gemini-company-form>');
    var directiveElement = compiledDirective($scope.company);
    angular.element('#edit-company-container').append(directiveElement);

    $scope.company.closeCompanyForm = function (didUpdate) {
      angular.element('#edit-company-container').empty();
      $scope.showNewCompanyButton = true;
      if (didUpdate) {
        $scope.loadCompanies();
      }
    };
    $scope.company.id = companyId;
  }

  $scope.addCompany = function () {
    $scope.showNewCompanyButton = false;
    addCompanyFormToDashboard();
  };

  $scope.addUser = function () {
    $scope.showNewUserButton = false;
    addUserFormToDashboard();
  };

  $scope.editUser = function (userId) {
    $scope.showNewUserButton = false;
    addUserFormToDashboard(userId);
  };

  $scope.getCompanies = function () {
    Company.getCompanies(function (companies) {
      $scope.selectedCompanyId = companies[0].id;
      $scope.companies = companies;
      $scope.loadAttemptsByCompanyId(companies[0].id);

    }, function (data, status) {
      console.log('error');
      console.log(data);
      console.log(status);
    });
  };

  $scope.loadAttemptsByCompanyId = function (companyId) {
    $scope.selectedCompanyId = companyId;
    if (companyId !== undefined) {
      Attempt.getAttemptsByCompanyId(companyId, function (attempts) {
        $scope.attempts = attempts;

      }, function (data, status) {
        console.log('error');
        console.log(data);
        console.log(status);
      });
    }
  };

  $scope.showNewUserButton = true;
  $scope.showNewCompanyButton = true;
  $scope.getCompanies();
  $scope.loadUsers();
});
