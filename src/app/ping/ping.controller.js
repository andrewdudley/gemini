'use strict';

angular.module('gemini')
.controller('PingCtrl', function (Ping) {

	Ping.pingDb(function() {
		console.log('Ping success.');

	}, function(data, status) {
	  	console.log('Ping error.');
	  	console.log('data: ', data);
		console.log('status: ', status);
	});
});
