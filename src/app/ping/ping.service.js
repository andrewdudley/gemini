'use strict';

angular.module('gemini')
.factory('Ping', function ($http, DARWIN_HOST) {

  return {

    ping: function (success, error) {

      $http.get(DARWIN_HOST + '/health-check/ping')
      .success(function () {
      })
      .error(function (data, status) {
        error(data, status);
      });
    },

    pingDb: function (success, error) {

      $http.get(DARWIN_HOST + '/health-check/ping')
      .success(function () {
      })
      .error(function (data, status) {
        error(data, status);
      });
    }

  };

});
