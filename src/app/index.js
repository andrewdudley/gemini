'use strict';

angular.module('gemini', ['ngMaterial', 'ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngRoute', 'ngStorage', 'ngMdIcons', 'gemini.config'])
  .config(function ($routeProvider, $httpProvider, $locationProvider) {

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });

    $routeProvider

      .when('/', {
        templateUrl: '/app/main/main.html',
        controller: 'MainCtrl',
        redirectSessionsTo: '/dashboard',
        publicAccess: true
      })

      .when('/dashboard', {
        templateUrl: '/app/dashboard/dashboard.html',
        controller: 'DashboardCtrl'
      })

      .when('/ping', {
        templateUrl: '/app/ping/pong.html'
      })

      .when('/ping-db', {
        templateUrl: '/app/ping/pong.html',
        controller: 'PingCtrl'
      })

      .when('/sign-out', {
        controller: 'SignOutCtrl'
      })

      .otherwise({
        redirectTo: '/'
      });


    // Token based REST API security
    $httpProvider.interceptors.push(function ($rootScope, $localStorage, $location, $q) {
      return {

        // Inject authorization token into every request
        request: function (config) {
          if ($rootScope.session) {
            config.headers.Authorization = 'bearer ' + $rootScope.session.accessToken;
          }
          return config;
        },

        // If response contains Authorization header, put it in rootScope for future requests
        response: function (response) {
          if (response && response.headers('Authorization') && $rootScope.session) {
            $rootScope.session.accessToken = response.headers('Authorization');
          }
          return response;
        },

        responseError: function (rejection) {
          if (rejection.status === 401 && $rootScope.session) {
            console.log('401 received from server and local session exists. Deleting session.');
            delete $localStorage.session;
            delete $rootScope.session;
            $location.path('/');
          }
          return $q.reject(rejection);
        }
      };
    });
  })
  .run(function ($rootScope, $injector, $localStorage) {

    $rootScope.$on('$routeChangeStart', function (event, next) {

      // Flash
      // Don't clear flash when being redirected
      // if (!next.redirectTo) {
      //   if ($rootScope.flash.persistOnce) {
      //     $rootScope.flash.persistOnce = false;
      //   } else {
      //     Flash.clear();
      //   }
      // }

      // Token based REST API security
      // If session exists in localStorage, put it in root scope
      if ($localStorage.session !== undefined) {
        if ($rootScope.session === undefined) {
          console.log('Found an existing session in local storage.');
          $rootScope.session = $localStorage.session;
        }
      } else {
        // $rootScope.loggedIn = true;
        delete $rootScope.session;
      }

      // Access control
      // First handle sessions
      if ($rootScope.session) {
        if (next.redirectSessionsTo) {
          console.log('Redirecting session to: ' + next.redirectSessionsTo);
          next.redirectTo = next.redirectSessionsTo;
          delete next.redirectSessionsTo;

//        } else if (next.access && next.access.indexOf($rootScope.session.user.role) === -1) {
//          $location.path('/');
        }

      // Then handle non-sessions
//      } else if (!next.publicAccess) {
//        $location.path('/');
      }

    });
  });
