'use strict';

var gulp = require('gulp');
var gulpNgConfig = require('gulp-ng-config');

gulp.task('config', function () {
  gulp.src('/etc/gemini/config.json')
  .pipe(gulpNgConfig('gemini.config'))
  .pipe(gulp.dest('src'))
  .pipe(gulp.dest('dist'));
});
